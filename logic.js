const Mikrotik = require('./mikrotik')
const q = require('q')

var add_user_secret = async (data)=>{
	var add = q.defer();
	   	try {

			var conex = await Mikrotik.connection(data);

			if (conex.con == true) {

				var connection = conex.conex;

				var user = await Mikrotik.search_user_secret(connection,{
					name: data.user
				})


				if (user.length > 0) {

					add.resolve({message:"user is already registered"});	
					await Mikrotik.close_connection(connection);
					
				}else{

					var aus = await Mikrotik.add_user_secret(connection,data)

					if (!aus.message) {

						var res = {
							id:aus[0].ret,
							user:data.user,
							message:"user successfully registered"
					   	};

						add.resolve(res);
					
						await Mikrotik.close_connection(connection);
					}else{

						add.resolve({message:aus.message});
						await Mikrotik.close_connection(connection);
					}
				}

			}else{
				add.resolve({message:conex.message});
			}
	   		
	   	} catch(err) {
		
	   		if (conex.con == true) {
	   			await Mikrotik.close_connection(conex.conex);
	   		}
			add.resolve({error:err.message});
	   	}

   return add.promise;
}


var delete_user_secret  = async (data)=>{
	var del = q.defer();
		try {

			var conex =  await Mikrotik.connection(data);

			if (conex.con == true) {
				var connection = conex.conex;

				var user = await Mikrotik.search_user_active(connection,{
					name:data.user
				})

				if (user.length > 0) {
					await Mikrotik.delete_user_active(connection,user[0]['.id'])
				}

				var del_u =await Mikrotik.delete_user_secret(connection,data.user)

				if (del_u == 'no such item') {

					var res = {
						message:'no such item'
					};

					del.resolve(res);

					await Mikrotik.close_connection(connection);
				}else{
					
					var res = {
						message:'user successfully removed'
					};

					del.resolve(res);

					await Mikrotik.close_connection(connection);
				}

			}else{
				del.resolve({message:conex.message});
			}
			
		} catch(err) {
			if (conex.con == true) {
	   			await Mikrotik.close_connection(conex.conex);
	   		}
			del.resolve({error:err.message});
		}

	return del.promise;
}


var list_user_secret = async (data)=>{
	var list = q.defer()
	   	try {

			var conex =  await Mikrotik.connection(data);

			if (conex.con == true) {

				var connection = conex.conex;

				var list_u = await Mikrotik.list_user_secret(connection)

				list.resolve(list_u);

				await Mikrotik.close_connection(connection);

			}else{
				list.resolve({message:conex.message});
			}
	   		
	   	} catch(err) {
			if (conex.con == true) {
	   			await Mikrotik.close_connection(conex.conex);
	   		}
			list.resolve({error:err.message});
	   	}

	 return list.promise;
}


var activate_user_secret = async (data)=>{
	var act = q.defer()
		try {
			var conex = await Mikrotik.connection(data);

			if (conex.con == true) {

				var connection = conex.conex;

				var user = await Mikrotik.search_user_secret(connection,{
					name: data.user
				})

				if (user.length > 0) {	

					var actv = await Mikrotik.activate_user_secret(connection,user[0]['.id'])

					act.resolve({message:"user activated"});
				}else{
					act.resolve({message:"user not found"});
				}
		
				await Mikrotik.close_connection(connection);

			}else{
				act.resolve({message:conex.message});
			}
		
		} catch(err) {
			if (conex.con == true) {
	   			await Mikrotik.close_connection(conex.conex);
	   		}
			act.resolve({error:err.message});
		}

	return act.promise;
}


var disable_user_secret = async (data)=>{
	var dis = q.defer()
		try {
			conex =  await Mikrotik.connection(data);

			if (conex.con == true) {

				var connection = conex.conex;

				var user = await Mikrotik.search_user_secret(connection,{
					name: data.user
				})

				if (user.length > 0) {

					await Mikrotik.disable_user_secret(connection,user[0]['.id'])

					dis.resolve({message:"user disabled"});
					
				}else{
					dis.resolve({message:"user not found"});
				}

				var user2 = await Mikrotik.search_user_active(connection,{
					name: data.user
				})

				if (user2.length > 0) {
					 await Mikrotik.delete_user_active(connection,user[0]['.id'])
				}
			
				await Mikrotik.close_connection(connection);

			}else{
				dis.resolve({message:conex.message});
			}

		} catch(err) {
			if (conex.con == true) {
	   			await Mikrotik.close_connection(conex.conex);
	   		}
			dis.resolve({error:err.message});
		}

	return dis.promise;	
}


module.exports = {	
	add_user_secret:add_user_secret,
	delete_user_secret:delete_user_secret,
	list_user_secret:list_user_secret,
	activate_user_secret:activate_user_secret,
	disable_user_secret:disable_user_secret
}
