const express = require('express');
const parser = require('body-parser');
const logic = require('./logic');
const app = express();

app.set('port',process.env.PORT || 5000);
app.use(parser.json());


/////////////add user///////////////////

app.post('/add_user_secret', async (request,response)=> {
		var token_api = request.headers['x-api-key'];

		if (validate(token_api)==true) {
			let dat;

	    	let data = (Object.entries(request.body).length === 0) ? request.query : request.body;
		  
			dat =  await logic.add_user_secret(data);

		    response.send(dat);
		}else{
			response.status(401).send({message:'x-api-key Unauthorized Failed'})
		}
})

/////////////delete user///////////////////

app.post('/delete_user_secret', async (request,response)=> {
  		var token_api = request.headers['x-api-key'];

  		if (validate(token_api)==true) {
			let dat;

	    	let data = (Object.entries(request.body).length === 0) ? request.query : request.body;
		  
			dat =  await logic.delete_user_secret(data);

		    response.send(dat);
		}else{
  			response.status(401).send({message:'x-api-key Unauthorized Failed'})
  		}

 })

/////////////list user///////////////////

app.post('/list_user_secret', async (request,response)=> {
  		var token_api = request.headers['x-api-key'];

  		if (validate(token_api)==true) {
			let dat;

	    	let data = (Object.entries(request.body).length === 0) ? request.query : request.body;
		  
			dat =  await logic.list_user_secret(data);

		    response.send(dat);
		}else{
  			response.status(401).send({message:'x-api-key Unauthorized Failed'})
  		}

 })

/////////////activate user///////////////////

app.post('/activate_user_secret', async (request,response)=> {
		var token_api = request.headers['x-api-key'];

	  	if (validate(token_api)==true) {	
			let dat;

	    	let data = (Object.entries(request.body).length === 0) ? request.query : request.body;
		  
			dat =  await logic.activate_user_secret(data);

		    response.send(dat);
		}else{
  			response.status(401).send({message:'x-api-key Unauthorized Failed'})
  		}

 })

 /////////////disable user///////////////////

app.post('/disable_user_secret', async (request,response)=> {
		var token_api = request.headers['x-api-key'];
  	
  		if (validate(token_api)==true) {
			let dat;

	    	let data = (Object.entries(request.body).length === 0) ? request.query : request.body;
		  
			dat =  await logic.disable_user_secret(data);

		    response.send(dat);
		}else{
  			response.status(401).send({message:'x-api-key Unauthorized Failed'})
  		}

 })

var validate = (data)=>{
	var token = 'f68c60525ad1fc64dbb468522f7a63bd73f1a72aa7a02a52b2a53f638fae2330';
	if (data ==token) {
		return true
	}else{
		return false
	}

}


app.use(express.static(__dirname + '/public'));

app.listen(app.get('port'), () =>
	console.log(`¡server up!`),
);
