const MikroNode = require('node-routeros').RouterOSAPI;
const q = require('q')

/**
  * Función para conectar al mk.
**/
var connection = async (config)=>{
	var con = q.defer();
	try {
		var conn = new MikroNode({
				host:config.conn.host, 
				user:config.conn.user_con, 
				password:config.conn.password_con, 
				port:config.conn.port
			});	

		await conn.connect() .then((connection) => {
			con.resolve({con:true,conex:connection});
		})
	    .catch((err) => {
			con.resolve({con:false, message:err.message});	
	    });
	} catch(err) {
		con.resolve({con:false, message:err.message});	
	}
	return con.promise;
}

/**
  * Función para Listar los usuarios secret.
**/
var list_user_secret = async (connection)=>{
	var lus = q.defer();
	try	{
		var values = await connection.write('/ppp/secret/print')
		lus.resolve(values);
	}catch(err){
		lus.resolve(err.message);
	}
	return lus.promise;
}

/**
   * Función para listar los usuarios activos.
**/
var list_user_active = async (connection)=>{
	var lua = q.defer();
	try	{
		var values = await connection.write('/ppp/active/print')
		lua.resolve(values);
	}catch(err){
		lua.resolve(err.message);
	}
	return lua.promise;
}

/**
  * Función para buscar los usuarios activos en el listado correspondiente.
**/
var search_user_active = async (connection,data)=>{
	var bua = q.defer();
	try{
		var list_user = await list_user_active(connection);
		
		var users_filter = list_user.filter( (user) => {
			var valid = true;
			for (var key in data){
				var value = data[key];
				valid = valid && value == user[key];
			}
			return valid;
		});
		bua.resolve(users_filter);
	}catch(err){
		bua.resolve(err.message);
	}
	return bua.promise;
}

/**
  * Función para buscar usuario en el listado correspondiente.
**/
var search_user_secret = async (connection,data)=>{
	var bus = q.defer();
	try{
		var list_user = await list_user_secret(connection);
		var users_filter = list_user.filter( (user) => {
			var valid = true;	
			for (var key in data){
				var value = data[key];
				valid = valid && value == user[key];
			}
			return valid;
		});
		bus.resolve(users_filter);	
	}catch(err){
		bus.resolve(err.message);	
	}
	return bus.promise;
}

/**
  * Función para eliminar un usuario activo.
**/
var delete_user_active = async(connection,id)=>{
	var eua = q.defer();
	try	{
		var config = ['=.id='+id];
		var values = await connection.write('/ppp/active/remove',config)
		eua.resolve(values);
	}catch(err){
		eua.resolve(err.message);	
	}
	return eua.promise;
}

/**
  * Función para eliminar un usuario secret.
**/
var delete_user_secret = async (connection,id)=>{
	var eus = q.defer();
	try	{
		var config = ['=.id='+id];
		var values = await connection.write('/ppp/secret/remove',config)
		eus.resolve(values);
	}catch(err){
		eus.resolve(err.message);
	}
	return eus.promise;
}

/**
  * Función para agregar un usuario secret.
**/
var add_user_secret = async (connection,data)=>{
	var aus = q.defer();

	var remote_key = '=remote-address='
	var local_key = '=local-address='

	try	{
		var config_secret = [
			'=name='+data.user,
	    	'=password='+data.password,
	    	'=service='+data.service,
	    	'=profile='+data.profile
		];
    	
		if (data.remote_address !="" && data.remote_address != undefined) {
    		config_secret[remote_key] = data.remote_address;
		}

		if (data.local_address !="" && data.local_address != undefined) {
    		config_secret[local_key] = data.local_address;
		}

    	var response = await connection.write('/ppp/secret/add', config_secret);
    	aus.resolve(response);	
	}catch(err){
		aus.resolve(err.message);	
	}
	return aus.promise;
}

/**
  * Función para activar un usuario secret.
**/
var activate_user_secret = async (connection,id)=>{
	var aus = q.defer();
	try	{
				
		var config = [
			'=.id='+id,
			'=disabled=no'
		];

		var values = await connection.write('/ppp/secret/set',config)
		aus.resolve(values);
	}catch(err){
		aus.resolve(err.message);
	}
	return aus.promise;
}

/**
  * Función para desactivar un usuario secret.
**/
var disable_user_secret = async (connection,id)=>{
	var dus = q.defer();
	try	{
		
		var config = [
			'=.id='+id,
			'=disabled=yes'
		];
		
		var values = await connection.write('/ppp/secret/set',config)
		dus.resolve(values);
	}catch(err){
		
		dus.resolve(err.message);
	}
	return dus.promise;
}

/**
  * Función para cerrar connection.
**/
var close_connection = async (connection)=>{
	var cc = q.defer();
	try{
		var ccc = await connection.close();
		cc.resolve(ccc)
	}catch(err){
		cc.resolve(err.message);
	}
	return cc.promise;
}


module.exports ={
	connection:connection,
	list_user_secret:list_user_secret,
	list_user_active:list_user_active,
	search_user_active:search_user_active,
	search_user_secret:search_user_secret,
	delete_user_active:delete_user_active,
	delete_user_secret:delete_user_secret,
	add_user_secret:add_user_secret,
	activate_user_secret:activate_user_secret,
	disable_user_secret:disable_user_secret,
	close_connection:close_connection
}